//
//  LPSAppDelegate.h
//  LPSUIKit
//
//  Created by Pituk Kaewsuksai on 08/13/2016.
//  Copyright (c) 2016 Pituk Kaewsuksai. All rights reserved.
//

@import UIKit;

@interface LPSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
