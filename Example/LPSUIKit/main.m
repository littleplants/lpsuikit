//
//  main.m
//  LPSUIKit
//
//  Created by Pituk Kaewsuksai on 08/13/2016.
//  Copyright (c) 2016 Pituk Kaewsuksai. All rights reserved.
//

@import UIKit;
#import "LPSAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LPSAppDelegate class]));
    }
}
