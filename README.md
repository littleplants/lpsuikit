# LPSUIKit

[![CI Status](http://img.shields.io/travis/Pituk Kaewsuksai/LPSUIKit.svg?style=flat)](https://travis-ci.org/Pituk Kaewsuksai/LPSUIKit)
[![Version](https://img.shields.io/cocoapods/v/LPSUIKit.svg?style=flat)](http://cocoapods.org/pods/LPSUIKit)
[![License](https://img.shields.io/cocoapods/l/LPSUIKit.svg?style=flat)](http://cocoapods.org/pods/LPSUIKit)
[![Platform](https://img.shields.io/cocoapods/p/LPSUIKit.svg?style=flat)](http://cocoapods.org/pods/LPSUIKit)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

LPSUIKit is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "LPSUIKit"
```

## Author

Pituk Kaewsuksai, littleplantstudio@gmail.com

## License

LPSUIKit is available under the MIT license. See the LICENSE file for more info.
